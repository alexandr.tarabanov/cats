import React, {Component} from 'react'
import Pack from './components/Packages/Pack.js'
import {cats} from './components/const'


class App extends Component {
    
    render() {
        return (
            <div className="o-app">
                <span>Ты сегодня покормил кота?</span>
                <Pack cats={cats} />
            </div>
        )
    }
}

export default App;
