import React from 'react'
import PropTypes from 'prop-types'


class Pack extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.mousePosition = this.mousePosition.bind(this)
    }
    
    Numeral(number, titles) {
        let cases = [2, 0, 1, 1, 1, 2];
        let numer = titles[
            (number % 100 > 4 && number % 100 < 20)
                ? 2
                : cases[(number % 10 < 5) ? number % 10 : 5]
            ]
        return <React.Fragment>{numer}</React.Fragment>
    }
    
    NumOfGifts(props) {
        return (
            <span>
                <b>{props} </b>
                {this.Numeral(props, ['порция', 'порции', 'порций'])}
            </span>
        )
    }
    
    CountOfPresents(props) {
        return (
            <span>
                <b>{props} </b>
                {this.Numeral(props, ['мышь', 'мыши', 'мышей'])} в подарок
            </span>
        )
    }
    
    mousePosition = (e) => {
        
        let target = e.currentTarget.querySelector('.o-cats-pack__select')
        
        if (target != null) {
            target.classList.add('active')
        }
    }
    
    handleSubmit(...props) {
        
        // вот так выбирать аргументы мне не нравится, но пока не придумал, как сделать лучше и правильнее
        const name = props[0]
        const action = props[1]
        const item = props[2]
        const e = props[3]
        
        e.preventDefault()
        
        let current = e.currentTarget.querySelector('.o-cats-pack__content')
        
        current.classList.toggle('o-cats-pack__select')
        
        if (!current.classList.contains('o-cats-pack__select')) {
            current.classList.remove('active')
        }
        
        // меняем текст вверху упаковки
        let span = current.querySelector('span')
        span.text = item.selectedText
        
        span.innerHTML = span.innerHTML === name ? item.selectedText : name
        
        // меняем текст под упаковкой
        let curr = current.nextSibling
        
        curr.innerHTML = action === curr.textContent.substr(0, action.length)
            ? item.description
            : `${action}, <span>купи.</span>`
    }
    
    render() {
        
        const {name, action, content} = this.props.cats
        
        
        const Act = (props) => {
            
            let act = !props.disabled
                ? <div>{action}, <span>купи.</span></div>
                : <div className="sorrow">Печалька, {props.name} закончился.</div>
            
            return <div className="o-cats-pack__link">{act}</div>
        }
        
        
        const items = content.map(item => {
            
            // замена точки на запятую
            let weight = item.weight.toString();
            weight = weight.replace('.', ',')
            
            return (
                <React.Fragment key={item.id}>
                    <div className="o-cats-pack__items"
                         onClick={!item.disabled ? this.handleSubmit.bind(this, name, action, item) : null}
                         onMouseLeave={this.mousePosition}
                    >
                        
                        {/* использую bind, чтобы передать вместе с this еще и аргументы  */}
                        <div className={`o-cats-pack__content ${item.disabled}`}>
                            
                            <div className="o-cats-pack__content-base">
                                
                                <span>{name}</span>
                                
                                <h2>{item.baseName}
                                    <small>{item.name}</small>
                                </h2>
                                <div className="o-cats-pack__desc">
                                    
                                    {/* количество упаковок*/}
                                    {this.NumOfGifts(item.pics)}
                                    
                                    {/* количество мышей в подарок */}
                                    {this.CountOfPresents(item.present)}
                                    
                                    {/* если значение присутствует, то отображаем */}
                                    {item.customer ? <span>{item.customer}</span> : ''}
                                </div>
                                <div className="o-cats-pack__weight">
                                    <span>{weight}</span><span>кг</span>
                                </div>
                            </div>
                        </div>
                        
                        {/* сообщение внизу карточки о том, что продукт закончился или нет */}
                        <Act disabled={item.disabled} name={item.name} />
                    
                    </div>
                </React.Fragment>
            )
        })
        
        
        return <div className="o-cats-pack__items-wrapper">{items}</div>
    }
    
}

Pack.propTypes = {
    name: PropTypes.string,
    action: PropTypes.string,
    content: PropTypes.shape({
        baseName: PropTypes.string,
        weight: PropTypes.number,
        pics: PropTypes.number,
        description: PropTypes.string,
        selectedText: PropTypes.string,
        customer: PropTypes.string,
        disabled: PropTypes.bool,
    }),
    act: PropTypes.string,
    current: PropTypes.element,
    target: PropTypes.element
}


export default Pack

