export const cats = {
    name: 'Сказочное заморское яство',
    action: 'Чего сидишь? Порадуй котэ',
    content: [
        {
            id: 1,
            baseName: 'Нямушка',
            name: 'с фуа-гра',
            pics: 10,
            present: 1,
            weight: .5,
            customer: '',
            description: 'Печень утки разварная с артишоками',
            selectedText: 'Котэ не одобряет?',
            disabled: ''
        },
        {
            id: 2,
            baseName: 'Нямушка',
            name: 'с рыбой',
            pics: 40,
            present: 2,
            weight: 2,
            customer: '',
            description: 'Головы щучьи с чесноком да свежайшая сёмгушка',
            selectedText: 'Котэ не одобряет?',
            disabled: ''
        },
        {
            id: 3,
            baseName: 'Нямушка',
            name: 'с курой',
            pics: 100,
            present: 5,
            weight: 5,
            customer: 'Заказчик доволен',
            description: 'Филе из цыплят с трюфелями в бульоне',
            selectedText: 'Котэ не одобряет?',
            disabled: 'disabled'
        }
    ]
}
